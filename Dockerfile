FROM ubuntu:18.04
LABEL authors="Kenn Bro, Sebastian Kennedy"

# Use bash for the shell
SHELL ["bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive

# Upgrade distro
RUN \
    apt-get update && \
    apt-get install -y aptitude && \
    aptitude upgrade -yq

# Install essentials
RUN aptitude install -yq build-essential wget curl git git-core apt-utils

# Install Node
RUN \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    aptitude install -yq nodejs && \
    node -v && \
    npm -v

# Install Redis
RUN \
  cd /tmp && \
  wget http://download.redis.io/redis-stable.tar.gz && \
  tar xvzf redis-stable.tar.gz && \
  cd redis-stable && \
  make && \
  make install

WORKDIR /opt

# Clone iKy
RUN git clone https://gitlab.com/kennbroorg/iKy.git

# Install python backend dependencies
WORKDIR /opt/iKy
RUN aptitude install -yq python-pip python-celery python-celery-common
RUN pip2 install -U pip
RUN pip2 install -r requirements.txt

# Install node frontend dependencies
WORKDIR /opt/iKy/frontend
RUN sed -i -e 's/ng serve/ng serve --host 0.0.0.0/g' package.json
RUN npm install

# EXPOSE ports
EXPOSE 4200
EXPOSE 5000

# Supervisor installation &&
# Create directory for child images to store configuration in
RUN aptitude install -yq supervisor && \
  mkdir -p /var/log/supervisor && \
  mkdir -p /etc/supervisor/conf.d
# Supervisor base configuration
ADD supervisor.conf /etc/supervisor.conf
RUN mkdir /opt/log

# Copy wake-up-iKy script
# RUN aptitude install -yq screen
# WORKDIR /opt/iKy

# COPY wake-up-iKy.sh .
# RUN chmod +x wake-up-iKy.sh
# RUN ./wake-up-iKy.sh

# RUN screen -A -m -d -S screen_redis-server redis-server
# RUN cd /opt/iKy/backend && \
#     screen -A -m -d -S screen_celery ./celery.sh
# RUN cd /opt/iKy/backend && \
#     screen -A -m -d -S screen_backend python app.py -i 0.0.0.0
# RUN cd /opt/iKy/frontend && \
#     screen -A -m -d -S screen_frontend npm start

# RUN mkdir /opt/log
# RUN redis-server > opt/log/redis.log 2>&1 &
# RUN cd /opt/iKy/backend && \
#     ./celery.sh > /opt/log/celery.log 2>&1 &
# RUN cd /opt/iKy/backend && \
#     python app.py -i 0.0.0.0 > /opt/log/app.log 2>&1 &
# RUN cd /opt/iKy/frontend && \
#     npm start > /opt/log/frontend.log 2>&1 &

CMD ["supervisord", "-c", "/etc/supervisor.conf"]
